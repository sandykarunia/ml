import tensorflow as tf
import matplotlib.pyplot as plt

class clazz(tf.keras.callbacks.Callback):
    def plot(self):
        plt.plot(self.losses)
        plt.plot(self.val_losses)
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'val'], loc='upper left')
        plt.show()

    def on_train_begin(self, logs={}):
        self.losses = []
        self.val_losses = []
        self.epochCount = 0

    def on_train_end(self, logs={}):
        return

    def on_epoch_begin(self, epoch, logs={}):
        return

    def on_epoch_end(self, epoch, logs={}):
        self.losses.append(logs.get('loss'))
        self.val_losses.append(logs.get('val_loss'))
        self.epochCount += 1
        print("epoch..." + str(self.epochCount))
        if self.epochCount % 10 == 0:
            self.plot()
        return

    def on_batch_begin(self, batch, logs={}):
        return

    def on_batch_end(self, batch, logs={}):
        return