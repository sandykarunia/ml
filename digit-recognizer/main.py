# https://www.kaggle.com/c/digit-recognizer/data

import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import time as time
import sys

from utils import callback_print_histories

np.random.seed(int(time.time()))

# load data
train_data_set = np.loadtxt('train.csv', delimiter=',', skiprows=1)
print("train data set " + str(train_data_set.shape))
Y = train_data_set[:, :1]
X = train_data_set[:, 1:]
print("X " + str(X.shape))
print("Y " + str(Y.shape))

# standardize input
# X = np.floor(X / 64)
mean = X.mean().astype(np.float32)
std = X.std().astype(np.float32)
print("mean: " + str(mean))
print("std: " + str(std))
X = (X - mean) / std
print("X[5]: " + str(X[5]))

# convert labels to be one-hot vector
Y_oh = tf.keras.utils.to_categorical(Y, num_classes=10)
print("Y[5]: " + str(Y[5]))
print("Y_oh[5]: " + str(Y_oh[5]))

model = tf.keras.Sequential()
model.add(tf.keras.layers.Dense(128, input_dim=784, activation='relu'))
model.add(tf.keras.layers.Dropout(0.25))
model.add(tf.keras.layers.Dense(64, activation='relu'))
model.add(tf.keras.layers.Dropout(0.25))
model.add(tf.keras.layers.Dense(10, activation='softmax'))

model.compile(loss='categorical_crossentropy', optimizer=tf.keras.optimizers.Adam(lr=0.001), metrics=['accuracy'])

model.summary()

model.fit(X, Y_oh, epochs=100, validation_split=0.3, callbacks=[callback_print_histories.clazz()], use_multiprocessing=1, verbose=0)

# evaluate the model
scores = model.evaluate(X, Y_oh)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

# test data
test_data_set = np.loadtxt('test.csv', delimiter=',', skiprows=1)
prediction_oh = model.predict([test_data_set])
prediction = prediction_oh.argmax(axis=-1)

# convert to str
submission = "ImageId,Label\n"
for i in range(len(prediction)):
    submission += str(i+1) + "," + str(prediction[i]) + "\n"

# save to submission.csv
with open("submission.csv", "w") as f:
    f.write(submission)

# ask for some test images index
while True:
    idx = int(raw_input("Digit index? (-1 to exit)"))
    if idx == -1:
        break
    img = test_data_set[idx].reshape((28, 28))
    plt.imshow(img, cmap='gray')
    plt.title("prediction: " + str(prediction[idx]))
    plt.show()
